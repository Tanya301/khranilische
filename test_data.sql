start transaction;

insert into users (username, password, email, last_login) values
    ('Tanya', md5('hellosklad'), 'tanya@samokhvalov.com', now()),
    ('User', md5('pass'), 't@samokhvalov.com', now()),
    ('cat', md5('123'), 'tanay@samokhvalov.com', now()),
    ('nik', md5('OdaOda'), 'nik@postgres.ai', now()),
    ('tola', md5('123'), 'andr.toshachakov@gmail.com', now())
    ('test', md5('123'), 'test@gmail.com', now());

insert into collections (name, description, user_id, parent_id) values
    ('Universities', 'Collection for soon-to-be university students', 1),
    ('Stanford', 'Information for people applying to Stanford university', 1, 1),
    ('Berkeley', 'Information for people applying to Berkeley university', 1, 1);

insert into posts (name, description, collection_id, url, user_id) values
    ('Stanford majors', 'Major list at Stanford', 2, ' https://majors.stanford.edu/', 1),
    ('Stanford international', 'Information about international admissions at Stanford', 2, 'https://admission.stanford.edu/apply/international/index.html', 1),
    ('Stanford application deadlines', 'Application deadlines for admissions at Stanford', 2, 'https://admission.stanford.edu/apply/deadlines/', 1),
    ('Berkeley requirements', 'Requirements for people applying to Berkeley', 3, 'https://admissions.berkeley.edu/freshmen-requirements', 1),
    ('Berkeley majors', 'List of majors ar Berkeley', 3, 'https://admissions.berkeley.edu/academic-programs-majors', 1),
    ('Berkeley deadlines', 'Application deadlines for people applying at Berkeley', 3, 'https://admissions.berkeley.edu/dates-deadlines', 1);

commit;
