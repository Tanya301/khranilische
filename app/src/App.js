import * as React from "react";
import { fetchUtils, Admin, Resource, ListGuesser, EditGuesser } from 'react-admin';
//import jsonServerProvider from 'ra-data-json-server';
import postgrestRestProvider, { authProvider } from '@raphiniert/ra-data-postgrest';
import { PostList, PostEdit, PostCreate, PostShow } from './posts';
import { UserList, UserEdit, UserCreate } from './users';
import { CollectionList, CollectionEdit, CollectionCreate, CollectionShow } from './collections';
import PostIcon from '@material-ui/icons/Book';
import UserIcon from '@material-ui/icons/Group';
import CollectionsBookmarkIcon from '@material-ui/icons/CollectionsBookmark';
import Dashboard from './dashboard'
//import authProvider from './authProvider'
//import LoginPage from './loginPage';

const httpClient = (url, options = {}) => {
	if (!options.headers) {
		options.headers = new Headers({ Accept: 'application/json' });
	}
	const { token } = JSON.parse(localStorage.getItem('me'));
	options.headers.set('Authorization', `Bearer ${token}`);
	return fetchUtils.fetchJson(url, options);
};

const dataProvider = postgrestRestProvider('https://tan.dblab.dev/rest', httpClient);

const App = () => (
	<Admin /*loginPage={LoginPage}*/ dashboard={Dashboard} authProvider={authProvider} dataProvider={dataProvider}>
	<Resource name="posts" list={PostList} show={PostShow} edit={PostEdit} create={PostCreate} icon={PostIcon}/>
	<Resource name="collections" list={CollectionList} show={CollectionShow} edit={CollectionEdit} create={CollectionCreate} icon={CollectionsBookmarkIcon}/>
	<Resource name="users" list={UserList} icon={UserIcon}/>
	</Admin>
);

export default App;
