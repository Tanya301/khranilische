import * as React from "react";
import { Card, CardContent, CardHeader } from '@material-ui/core';

export default () => (
    <Card>
        <CardHeader title="Welcome to KHRANILISCHE" />
        <CardContent>
            <p>
                Khranilische [khɹeɪ’nɪlɪsʃe] is a project, which helps you keep your knowledge organized.
                You can find information that is already on the platform or add new info using the
                "Create" button.
            </p>
            <p>
                Should you have any questions, plase contact me directly:
                <ul>
                    <li>via email <a href="mailto:sch444@samokhvalov.com">sch444@samokhvalov.com</a></li>
                    <li>via instagram <a href="https://instagram/tan4eeg">@tan4eeg</a></li>
                </ul>
            </p>
            <p>GitLab: <a href={"https://gitlab.com/Tanya301/khranilische"}>khranilische</a></p>
        </CardContent>
    </Card>
);
