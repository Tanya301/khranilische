import * as React from "react";
import {
    List,
    ReferenceManyField,
    Datagrid,
    Show,
    ShowButton,
    SimpleShowLayout,
    TextField,
    ReferenceField,
    EditButton,
    Edit,
    Create,
    SimpleForm,
    ReferenceInput,
    SelectInput,
    TextInput,
} from 'react-admin';

export const CollectionList = props => (
    <List {...props}>
        <Datagrid>
            <TextField source="name" />
            <TextField source="description" />
            <ReferenceField source="user_id" reference="users">
                <TextField source="username" />
            </ReferenceField>
            <ReferenceField source="parent_id" reference="collections">
                <TextField source="name" />
            </ReferenceField>
            <EditButton />
            <ShowButton />
        </Datagrid>
    </List>
);

export const CollectionEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput multiline source="description" />
            <ReferenceInput source="parent_id" reference="collections">
                <SelectInput optionText="name" />
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);

export const CollectionCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput multiline source="description" />
            <ReferenceInput source="parent_id" reference="collections">
                <SelectInput optionText="name" />
            </ReferenceInput>
        </SimpleForm>
    </Create>
);

export const CollectionShow = props => (
  <Show {...props}>
    <SimpleShowLayout>
      <TextField source="name" />
      <TextField source="description" />
      <ReferenceField source="user_id" reference="users">
          <TextField source="username"/>
      </ReferenceField>
      <ReferenceManyField
        reference="posts"
        target="collection_id"
        label="Posts"
      >
        <Datagrid>
          <TextField source="name" />
          <TextField source="description" />
        </Datagrid>
      </ReferenceManyField>
      <ReferenceManyField
        reference="collections"
        target="parent_id"
        label="Collections"
      >
        <Datagrid>
          <TextField source="name" />
          <TextField source="description" />
        </Datagrid>
      </ReferenceManyField>
    </SimpleShowLayout>
  </Show>
);
