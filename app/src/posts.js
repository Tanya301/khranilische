import * as React from "react";
import {
    List,
    Show,
    SimpleShowLayout,
    Datagrid,
    TextField,
    ReferenceField,
    EditButton,
    ShowButton,
    Edit,
    Create,
    SimpleForm,
    ReferenceInput,
    SelectInput,
    TextInput,
    Filter,
} from 'react-admin';
import MyUrlField from './MyUrlField';
import { useMediaQuery } from '@material-ui/core';

export const PostFilter = (props) => (
    <Filter {...props}>
        <TextInput label="Search" source="description" alwaysOn />
        <ReferenceInput label="User" source="id" reference="users" allowEmpty>
            <SelectInput optionText="username" />
        </ReferenceInput>
    </Filter>
);

export const PostList = props => ( 
    <List filters={<PostFilter />} {...props}>
        <Datagrid rowClick="show">
            <TextField source="name" />
            <TextField source="description" />
            <ReferenceField source="collection_id" reference="collections">
                <TextField source="name" />
            </ReferenceField>
            <ReferenceField source="user_id" reference="users">
                <TextField source="username" />
            </ReferenceField>
            <MyUrlField source="url" />
            <EditButton />
            <ShowButton />
        </Datagrid>
    </List>
);

export const PostEdit = props => (
    <Edit {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput multiline source="description" />
            <TextInput source="url" />
            <ReferenceInput source="collection_id" reference="collections">
                <SelectInput optionText="name" />
            </ReferenceInput>
        </SimpleForm>
    </Edit>
);

export const PostCreate = props => (
    <Create {...props}>
        <SimpleForm>
            <TextInput source="name" />
            <TextInput multiline source="description" />
            <TextInput source="url" />
            <ReferenceInput source="collection_id" reference="collections">
                <SelectInput optionText="name" />
            </ReferenceInput>
        </SimpleForm>
    </Create>
);

export const PostShow = (props) => (
    <Show {...props}>
        <SimpleShowLayout>
            <TextField source="name" />
            <TextField source="description" />
            <ReferenceField source="collection_id" reference="collections">
                <TextField source="name" />
            </ReferenceField>
            <ReferenceField source="user_id" reference="users">
                <TextField source="username" />
            </ReferenceField>
            <MyUrlField source="url" />
            <EditButton />
        </SimpleShowLayout>
    </Show>
)
