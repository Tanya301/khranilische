start transaction;

drop table if exists users cascade;
drop table if exists collections cascade;
drop table if exists posts cascade;
drop table if exists pendingUsers cascade;

create table users (
    id bigserial primary key,
    username varchar(20) unique not null,
    email varchar(100),
    password varchar(32) not null,
    last_login date
);

create table pendingUsers (
    id bigserial primary key,
    username varchar(20) unique not null,
    email varchar(100),
    uStatus boolean,
    timeCreated timestamp
);

create table collections (
    id bigserial primary key,
    name varchar(100) not null,
    description varchar(500),
    parent_id bigint references collections(id),
    user_id bigint references users(id) not null
);

create table posts (
    id bigserial primary key,
    name varchar(100) not null,
    description varchar(500),
    collection_id bigint references collections(id),
    url text,
    user_id bigint not null references users(id) not null
);

---- API ----
drop schema if exists api cascade;
create schema api;
grant usage on schema api to anon;
grant usage on schema api to reguser;

drop view if exists api.collections;
create view api.collections as select * from collections;
grant select on api.collections to anon;
revoke update, insert, delete on api.collections from anon;
grant select, update, insert, delete on api.collections to reguser;

drop view if exists api.posts;
create view api.posts as select * from posts;
grant select on api.posts to anon;
revoke update, insert, delete on api.posts from anon;
grant select, update, insert, delete on api.posts to reguser;

drop view if exists api.users;
create view api.users as select id, username from users;
grant select, insert on api.users to anon;
revoke update, insert, delete on api.users from anon;
grant select, update, insert, delete on api.users to reguser;

drop view if exists api.pendingUsers;
create view api.pendingUsers as select id, username, uStatus, timeCreated from pendingUsers;
grant select, update, insert on api.pendingUsers to anon;
revoke delete on api.pendingUsers from anon;
grant select, update, insert, delete on api.users to reguser;

grant all on posts_id_seq to anon;
grant all on users_id_seq to anon;
grant all on collections_id_seq to anon;
grant all on pendingUsers_id_seq to anon;

grant all on posts_id_seq to reguser;
grant all on users_id_seq to reguser;
grant all on collections_id_seq to reguser;
grant all on pendingUsers_id_seq to reguser;

commit;
