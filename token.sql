  begin;
  drop schema if exists jwt cascade;
  drop extension if exists pgcrypto;
  create schema jwt;
  create extension pgcrypto;

  CREATE OR REPLACE FUNCTION jwt.url_encode(data bytea) RETURNS text LANGUAGE sql AS $$
      SELECT translate(encode(data, 'base64'), E'+/=\n', '-_');
  $$;


  CREATE OR REPLACE FUNCTION jwt.url_decode(data text) RETURNS bytea LANGUAGE sql AS $$
  WITH t AS (SELECT translate(data, '-_', '+/') AS trans),
      rem AS (SELECT length(t.trans) % 4 AS remainder FROM t) -- compute padding size
      SELECT decode(
          t.trans ||
          CASE WHEN rem.remainder > 0
            THEN repeat('=', (4 - rem.remainder))
            ELSE '' END,
      'base64') FROM t, rem;
  $$;


  CREATE OR REPLACE FUNCTION jwt.algorithm_sign(signables text, secret text, algorithm text)
  RETURNS text LANGUAGE sql AS $$
  WITH
    alg AS (
      SELECT CASE
        WHEN algorithm = 'HS256' THEN 'sha256'
        WHEN algorithm = 'HS384' THEN 'sha384'
        WHEN algorithm = 'HS512' THEN 'sha512'
        ELSE '' END AS id)  -- hmac throws error
  SELECT jwt.url_encode(hmac(signables, secret, alg.id)) FROM alg;
  $$;


  CREATE OR REPLACE FUNCTION jwt.sign(payload json, secret text, algorithm text DEFAULT 'HS256')
  RETURNS text LANGUAGE sql AS $$
  WITH
    header AS (
      SELECT jwt.url_encode(convert_to('{"alg":"' || algorithm || '","typ":"JWT"}', 'utf8')) AS data
      ),
    payload AS (
      SELECT jwt.url_encode(convert_to(payload::text, 'utf8')) AS data
      ),
    signables AS (
      SELECT header.data || '.' || payload.data AS data FROM header, payload
      )
  SELECT
      signables.data || '.' ||
      jwt.algorithm_sign(signables.data, secret, algorithm) FROM signables;
  $$;


  CREATE OR REPLACE FUNCTION jwt.verify(token text, secret text, algorithm text DEFAULT 'HS256')
  RETURNS table(header json, payload json, valid boolean) LANGUAGE sql AS $$
    SELECT
      convert_from(jwt.url_decode(r[1]), 'utf8')::json AS header,
      convert_from(jwt.url_decode(r[2]), 'utf8')::json AS payload,
      r[3] = jwt.algorithm_sign(r[1] || '.' || r[2], secret, algorithm) AS valid
    FROM regexp_split_to_array(token, '\.') r;
  $$;

  CREATE TYPE jwt.jwt_token AS (
    token text
  );

  drop function if exists api.jwt_test();
  CREATE FUNCTION api.jwt_test() RETURNS jwt.jwt_token AS $$
    SELECT jwt.sign(
      row_to_json(r), current_setting('jwt.secret')::text
    ) AS token
    FROM (
      SELECT
        'my_role'::text as role,
        extract(epoch from now())::integer + 300 AS exp
    ) r;
  $$ LANGUAGE sql security definer;
  grant execute on function api.jwt_test() to anon;


  create or replace function
  jwt.user_role(email text, password text) returns name
    language plpgsql
    as $$
  begin
    return (
      select 'reguser'
      from public.users
      where
        lower(users.email) = lower(user_role.email)
        and md5(user_role.password) = users.password
    );
  end;
  $$;

  -- login should be on your exposed schema
  create or replace function api.login(email text, password text) returns jwt.jwt_token as $$
  declare
    _role name;
    result jwt.jwt_token;
  begin
    -- check email and password
    select jwt.user_role(email, password) into _role;
    if _role is null then
      raise invalid_password using message = 'invalid user or password';
    end if;

    select jwt.sign(
        row_to_json(r), current_setting('jwt.secret')::text
      ) as token
      from (
        select _role as role, login.email as email,
          extract(epoch from now())::integer + 60*60 as exp
      ) r
      into result;
    return result;
  end;
  $$ language plpgsql security definer;
  grant execute on function api.login(text, text) to anon;
  
  create or replace function api.logout() returns text as $$
    select 'ok';
  $$ language sql;
  grant execute on function api.logout() to reguser;

  /* Utility function to get the ID of currently authenticated user */
  create or replace function jwt.get_user_id() returns int8 as $$
    select id
    from public.users
    where lower(users.email) = lower(current_setting('request.jwt.claim.email'))
  $$ language sql security definer;

  alter table collections alter column user_id set default jwt.get_user_id();  
  alter table posts alter column user_id set default jwt.get_user_id();

  commit;
