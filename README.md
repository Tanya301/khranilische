[[_TOC_]]

# Dependencies
## Docker
```shell
sudo apt-get update && sudo apt-get install -y \
  apt-transport-https \
  ca-certificates \
  curl \
  gnupg-agent \
  software-properties-common

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
  "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) \
  stable"

sudo apt-get update \
&& sudo apt-get install -y \
  docker-ce \
  docker-ce-cli \
  containerd.io \
  git \
  procps \
  vim \
  net-tools \
  dstat \
  jq \
  sysstat

# NVMe
sudo mkdir -p /disk
sudo mkfs.ext4 /dev/nvme1n1
sudo mount -o noatime -o data=writeback -o barrier=0 -o nobh \
  /dev/nvme1n1 /disk
df -hT
```

## NGINX + SSL
```shell
#export IP_OR_HOSTNAME="$(curl https://ipinfo.io/ip)"
export IP_OR_HOSTNAME="tan.dblab.dev"

sudo apt-get install -y nginx openssl

read -sp 'Enter custom password: ' YOUR_OWN_PASS

mkdir -p ~/ssl
cd ~/ssl

# TODO: Use https://github.com/suyashkumar/ssl-proxy instead.
# To generate certificates, use, for instance, Let's Encrypt
# (e.g. https://zerossl.com/free-ssl/#crt).
# Here we are generating a self-signed certificate.

openssl genrsa -des3 -passout pass:${YOUR_OWN_PASS} -out server.pass.key 2048
openssl rsa -passin pass:${YOUR_OWN_PASS} -in server.pass.key -out server.key
rm server.pass.key

# Will ask a bunch of questions which should be filled with answers.
openssl req -new -key server.key -out server.csr

openssl x509 -req -sha256 -days 365 -in server.csr -signkey server.key \
  -out server.crt

sudo mkdir -p /etc/nginx/ssl
sudo cp server.crt /etc/nginx/ssl
sudo cp server.key /etc/nginx/ssl

cat <<CONFIG > default
server {
  listen 443 ssl;

  ssl on;
  ssl_certificate /etc/nginx/ssl/server.crt;
  ssl_certificate_key /etc/nginx/ssl/server.key;
  add_header  Strict-Transport-Security "max-age=0;";

  server_name tan.dblab.dev;
  access_log /var/log/nginx/access.log;
  error_log /var/log/nginx/error.log;

  location /rest {
    rewrite ^/rest/?(.*) /\$1 break;
    proxy_set_header   X-Forwarded-For \$remote_addr;
    proxy_set_header   Host \$http_host;
    proxy_pass         "http://127.0.0.1:3000";
  }

  location /iframely {
    rewrite ^/iframely/?(.*) /\$1 break;
    proxy_set_header   X-Forwarded-For \$remote_addr;
    proxy_set_header   Host \$http_host;
    proxy_pass         "http://127.0.0.1:8061";
  }

  location / {
    proxy_set_header   X-Forwarded-For \$remote_addr;
    proxy_set_header   Host \$http_host;
    proxy_pass         "http://127.0.0.1:3001";
  }
}
CONFIG

sudo cp default /etc/nginx/sites-available/default

sudo systemctl restart nginx
```

# Containers
## Postgres
```shell
sudo mkdir -p /disk/pgdata
sudo docker run \
  -e PGDATA=/pgdata \
  -v /disk/pgdata:/pgdata \
  -e POSTGRES_PASSWORD=passpass \
  -p 5432:5432 \
  --detach \
  --name kh_postgres \
  postgres:13

sudo docker exec -it kh_postgres psql -U postgres -c 'create database sklad'
```

## PostgREST
```shell
echo 'db-uri       = "postgres://postgres:passpass@localhost:5432/sklad"
db-schema    = "api"
db-anon-role = "anon"' > /home/ubuntu/postgrest.conf

sudo docker run \
  -v /home/ubuntu/postgrest.conf:/etc/postgrest.conf \
  --net=host \
  --detach \
  --name kh_api \
  postgrest/postgrest
```

## iframely
```shell
#curl https://raw.githubusercontent.com/itteco/iframely/master/config.local.js.SAMPLE > iframely.local.js

sudo docker run \
  -it -p 8061:8061 \
  --name iframely \
  --detach \
  postgresai/iframely:latest
###
```
